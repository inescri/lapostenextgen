function ZoopSynthese() {

	var DATASET = {
		isc: {score: generateRandomScore(2), trend: generateRandomTrend(4)},
		pro: {score: generateRandomScore(2), trend: generateRandomTrend(2)},
		banque: {score: generateRandomScore(2), trend: generateRandomTrend(2), expanded: {
			data:  [generateRandomScore(), generateRandomScore(), generateRandomScore()], colors:  ["#fecb00", "#333","#fff"], categories: ["Satisfait", "Non satisfait", "Netrual"]
		}},
		financier: {nps: generateRandomScore(2), score:generateRandomScore()},
		comments: [{
			client: 'Catherine DE MEDECI', date: '15/02/2017',
			comment: 'Lorem ipsum. Bayang magiliw perlas ng silanganan. Alab ng puso \'di ka pasisiil. Sa dagat at bundok sa simoy at sa lingit mong bughaw. Aming ligaya na \'pag may mang-aapi.'
		}, {
			client: 'Marie DE GUISE', date: '16/02/2017',
			comment: 'Lorem ipsum. Dalaga binata may asawa byuda. Itim puti pula asul kayumangi dilaw. Lintik lang ang walang ganti. Pikachu use thunderbolt.'
		}, {
			client: 'Jean BAPTISTE', date: '16/02/2017',
			comment: 'Lorem ipsum. Sit dolor amet. Run rabbit, run. If you the think the work is done it\'s time to dig another one. Breath, breath into the air.'
		}],
		recontact: generateRandomScore()
	};

	// pussing the last value for realism
	DATASET.isc.trend.push(DATASET.isc.score);
	DATASET.pro.trend.push(DATASET.pro.score);
	DATASET.banque.trend.push(DATASET.banque.score);
	//get the percentage
	DATASET.banque.expanded.datePercent = getPercentage(DATASET.banque.expanded.data);

	createZRing('#centreFinancierNPS', DATASET.financier.nps, 130, ['#fecb00']);

	createBanquePie('#distro_banque', DATASET.banque.expanded);
	$('#ISCscore').text(DATASET.isc.score);
	$('#proScore').text(DATASET.pro.score);
	$('#banqueScore').text(DATASET.banque.score);
	$('#financierScore').text(DATASET.financier.score);
	
	// banque legends
	$('#distro_banque_legend .score.satisfied').text(DATASET.banque.expanded.datePercent[0]);
	$('#distro_banque_legend .score.dissatisfied').text(DATASET.banque.expanded.datePercent[1]);
	$('#distro_banque_legend .score.neutral').text(DATASET.banque.expanded.datePercent[2]);
	
	$('.card-recontacter h1').text(DATASET.recontact);
	
	// initializing the trends
	createTrends('#trendLine_ic', DATASET.isc.trend, ['nov-16', 'dec-16','jan-17', 'fev-17', 'mar-17']);
	createTrends('#trendLine_pro', DATASET.pro.trend, ['jan-17', 'fev-17', 'mar-17'])
	createTrends('#trendLine_banque', DATASET.banque.trend, ['jan-17', 'fev-17', 'mar-17']);

	var vhb = Handlebars.compile($('#tempplateVerbatims').html());
	var verbatims = vhb(DATASET);
	$('#verbatimsContainer').html(verbatims);

	$('.card-tile').parent().css('transition', '0.5s')

	$('.card-tile-clickable').on('click', function() {
		var cardID = $(this).attr('card-id');
		$('.card-tile, .card-tile-micro').removeClass('active');
		$('[card-id="'+cardID+'"]').addClass('active');
		expandCartTile(cardID);
	});

	function expandCartTile(card) {
		$view = $('#expandedCardView');
		$view.show();
		$('.card-tile .trend-line, .card-tile .hide-when-expanded').hide();
		$('.card-tile').parent().css('width', '100%').removeClass('col-md-4').addClass('col-md-12').appendTo('#expandedRightSideCardView');
		$('.card-tile.card-tile-focus').parent().appendTo('#expandedLeftSideCardView');

		$('#verbatimsContainer').find('[class*="col-"]').css('width', '100%');
		$('#verbatimsContainer').find('[class*="col-"]:gt(0)').hide();

		emulateSyntheseLoading(function() {
			$.ajax({
				url: getCardPath(card),
				success: function(res) {
					$view.html(res);
					initExpandedCard(card);
				},
				error: function(res) {
					$view.html('<h1>Page introuvable !</h1>')
				}
			});
		})
	}

	function getCardPath(card) {
		return 'pages/synthese/cards/' + card + '.html';
	}

	function generateRandomScore(decimalPlaces) {
		decimalPlaces = (!decimalPlaces)? 0 : decimalPlaces;
		return parseFloat(Math.random() * 100).toFixed(decimalPlaces);
	}

	function generateRandomTrend(len, decimalPlaces) {
		var trend = [];
		for (var i=0; i<len; i++) {
			trend.push(generateRandomScore(decimalPlaces));
		}
		return trend;
	}

	function emulateSyntheseLoading(callback) {
		$view = $('#expandedCardView');
		$view.html('<div class="spinner"><i class="zmdi zmdi-hc-spin zmdi-hc-2x zmdi-spinner"></i></div>');
		setTimeout(function() {
			callback();
		}, 1000);
	}

	function createTrends(container, data, labels) {
		var target = $(container)[0].getContext('2d');
		var myChart = new Chart(target, {
			type: 'line',
			options: {
				maintainAspectRatio: false,
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						ticks: {
							display: false,
							beginAtZero: true,
						}
					}],
					xAxes: [{
						ticks: {
							fontColor: '#fff',
							display: false
						}
					}]
				}
			},
			data: {
				labels: labels,
				datasets: [{
					label: 'Agence',
					lineTension: 0,
					backgroundColor: "rgba(255,255,255,0.0)",
					borderColor: "rgba(255,255,255,1)",
					fill: true,
					pointRadius: 4,
					pointBorderWidth: 2,
					pointBackgroundColor: '#4b7598',
					data: data
				}]
			}
		});
	}

	function createBanquePie(container, dataset) {
		var target = $(container)[0].getContext('2d');
		var myChart = new Chart(target, {
		  type: 'doughnut',
		  options: {
		  	legend: {display: false, position: 'bottom', labels: {fontColor:'#fff', fontSize: 11, boxWidth: 15}},
		  	cutoutPercentage: 75,
		  	responsive: false
		  },
		  data: {
		    labels: dataset.categories,
		    datasets: [{
		    	borderColor: 'rgba(0,0,0,0)',
		    	backgroundColor: dataset.colors,
		      	data: dataset.data
		    }]
		  }
		});
	}

	function createTrendsSignatures(data) {
		$.each(data, function(i, d) {
			$.each(d.questions, function(i, q) {
				var target = $('#trendLine_' + q.code)[0].getContext('2d');
				var myChart = new Chart(target, {
					type: 'line',
					options: {
						maintainAspectRatio: false,
						legend: {
							display: false
						},
						scales: {
							yAxes: [{
								ticks: {
									display: false,
									beginAtZero: false,
								}
							}]
						}
					},
					data: {
						labels: ['jan-17', 'fev-17', 'mar-17', 'avr-17', 'mai-17'],
						datasets: [{
							label: 'National',
							lineTension: 0,
							backgroundColor: "rgba(76,142,178,0.2)",
							borderColor: "rgba(76,142,178,1)",
							fill: false,
							data: [67, 78, 87, 88, 91]
						}, {
							label: 'Agence',
							lineTension: 0,
							backgroundColor: "rgba(247,190,32,0.2)",
							borderColor: "rgba(247,190,32,1)",
							fill: false,
							data: q.trend.me
						}]
					}
				});

			})
		});

		
	}
	
	function getPercentage(datarange) {
		var total = 0, percent = [];
		
		for (var i=0; i<datarange.length; i++) {
			var total = total + parseFloat(datarange[i]);
		}
		
		for (var i=0; i<datarange.length; i++) {
			var val = (parseFloat(datarange[i])/total) * 100;
			percent.push(parseFloat(val).toFixed(1) + '%');
		}
		
		return percent;
	}
	
	function initExpandedCard(card) {

	

		DATASET['clients-professionnels'] = { kpis:
		[{
			signature: 'Personnaliser la relation avec les clients Pros', 
			iscfocus: 'iscfocus',
			questions: [{ code: 'q19', title: 'Recommandation'}, { code: 'q8', title: 'Satisfaction globale'}]
		}, {
			signature: '1. Les 30 premi\350res secondes',
			questions: [
				{ code: 'q10', title: 'Accueil' }
			]
		}, {
			signature: '2. Servir les clients dans les meilleurs d\351lai',
			questions: [
				{ code: 'q13', title: 'Attente '}
			]
		}, {
			signature: '3. Etre encore plus attentifs \340 nos clients bancaires',
			questions: [
				{ code: 'q6', title: 'Offres bancaires' }
			]
		}, {
			signature: '4. Personnaliser la relation avec les clients Pros',
			questions: [
				{ code: 'q4', title: 'Conseillers Pro' },
				{ code: 'q15', title: 'Guichet Pro' },
				{ code: 'q16', title: 'Carte Pro' },
				{ code: 'q17', title: 'Conseil' },
				{ code: 'q18', title: 'Suivi' },
				{ code: 'q7', title: 'Services' }
			]
		}, {
			signature: '5. Etre pr\351sents \340 chaque instant',
			questions: [
				{ code: 'q11', title: 'Ecoute'},
				{ code: 'q12', title: 'Sens du service'}
			]
		}, {
			signature: '6. Proches, accessibles et connect\351s',
			questions: [
				{ code: 'q14', title: 'Automates'},
				{ code: 'q5', title: 'Offres en ligne'}
			]
		}, {
			signature: '7. L\'avis des nos clients nous importe',
			questions: [
				{ code: 'q22', title: 'Recontact'},
				{ code: 'q23', title: 'Recontact'}
			]
		}]};

		DATASET['banque-postale'] = {kpis: [{
			signature: 'Etre encore plus attentifs à nos clients bancaires',
			iscfocus: 'iscfocus',
			questions: [{ code: 'q20', title: 'Recommandation'}, { code: 'q6', title: 'Satisfaction globale'}]
		}, {
			signature: '1. Les 30 premières secondes',
			questions: [
				{ code: 'q11', title: 'Bureau' },
				{ code: 'q13', title: 'Accueil' },
				{ code: 'q2', title: 'Satisfaction bureau' }
			]
		}, {
			signature: '2. Servir les clients dans les meilleurs d\351lai',
			questions: [
				{ code: 'q12', title: 'Pontualit\351' },
				{ code: 'q8', title: 'Joignabilit\351' },
				{ code: 'q9', title: 'Disponibilit\351' }
			]
		}, {
			signature: '3. Etre encore plus attentifs \340 nos clients bancaires',
			questions: [
				{ code: 'q16', title: 'Aboutissement' },
				{ code: 'q20-1', title: 'Recommandation' },
				{ code: 'q22', title: 'Fid\351lisation' },
				{ code: 'q23', title: 'Intention client'}
			]
		}, {
			signature: '4. Etre pr\351sents \340 a chaque instant',
			questions: [
				{ code: 'q14', title: 'Ecoute' },
				{ code: 'q15', title: 'Conseil'}
			]
		}]};

		// generate random score to show
		if (DATASET[card]) {
			$.each(DATASET[card].kpis, function(i, d) {
				d.score = generateRandomScore();
				d.trend = generateRandomTrend(7);
				$.each(d.questions, function(i, s) {
					s.score = generateRandomScore();
					s.trend = {
						me: generateRandomTrend(4),
						parent: generateRandomTrend(5)
					}
					s.trend.me.push(s.score);
				})
			})
		}
		
		var hb = Handlebars.compile($('#templateSignatures').html());
		if (DATASET[card]) {
			var html = hb(DATASET[card]);
			$('div.signatures').html(html);
			createTrendsSignatures(DATASET[card].kpis);
		}

	}
}