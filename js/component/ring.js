function createZRing(elem, value, size, colors) {
	fvalue = parseFloat(value);
	var dataset = [
		{ label: 'filled', count: fvalue },
		{ label: 'not-filled', count: (100 - fvalue) }
	];

	var width = (!size)? 200: size;
	var height = (!size)? 200: size;
	var radius = Math.min(width, height) / 2;
	var fontSize = height / 6;
	
	if (!colors) {
		var color = d3.scaleOrdinal(d3.schemeCategory20b);
	} else {
		colors.push('rgba(0,0,0, 0.3)');
		var color = d3.scaleOrdinal()
			.range(colors);
	}

	var svg = d3.select(elem)
		.append('svg')
		.attr('width', width)
		.attr('height', height)
		.append('g')
		.attr('transform', 'translate(' + (width / 2) +
			',' + (height / 2) + ')');

	var arc = d3.arc()
		.innerRadius(radius - 10)
		.outerRadius(radius);

	var pie = d3.pie()
		.value(function(d) { return d.count; })
		.sort(null);

	var path = svg.selectAll('path')
		.data(pie(dataset))
		.enter()
		.append('path')
		.attr('d', arc)
		.attr('fill', function(d) {
			return color(d.data.label);
		})
		.transition().duration(1000)
		.attrTween("d", function (d) { 
				var start = {startAngle: 0, endAngle: 0};
				var interpolate = d3.interpolate(start, d);
				return function (t) {
						return arc(interpolate(t));
				};
		});

	svg.append('g')
		.attr('transform', 'translate('+(0 - fontSize )+', '+fontSize / 2+')')
		.append('text')
		.attr('class','xring-text-value')
		.attr('font-size', fontSize + 'px');

 var centerText = d3.select(elem + ' .xring-text-value')
		.text(value);
}



function createZPie(elem, data, size, colors) {
	var dataset = data

	var width = (!size)? 200: size;
	var height = (!size)? 200: size;
	var radius = Math.min(width, height) / 2;
	var fontSize = height / 8;
	
	if (!colors) {
		var color = d3.scaleOrdinal(d3.schemeCategory20b);
	} else {
		colors.push('rgba(0,0,0, 0.3)');
		var color = d3.scaleOrdinal()
			.range(colors);
	}

	var svg = d3.select(elem)
		.append('svg')
		.attr('width', width)
		.attr('height', height)
		.append('g')
		.attr('transform', 'translate(' + (width / 2) +
			',' + (height / 2) + ')');

	var arc = d3.arc()
		.innerRadius(radius - 10)
		.outerRadius(radius);

	var pie = d3.pie()
		.value(function(d) { return d.count; })
		.sort(null);

	var path = svg.selectAll('path')
		.data(pie(dataset))
		.enter()
		.append('path')
		.attr('d', arc)
		.attr('fill', function(d) {
			return color(d.data.label);
		})
		.transition().duration(1000)
		.attrTween("d", function (d) { 
				var start = {startAngle: 0, endAngle: 0};
				var interpolate = d3.interpolate(start, d);
				return function (t) {
						return arc(interpolate(t));
				};
		});
}

