(function() {
	angular
		.module('zoop.laposte')
		.controller('SyntheseExpandedController', SyntheseExpandedController);
	
	SyntheseExpandedController.$inject = ['SyntheseService', 'PeriodService', 'PerimeterService', '$state', '$scope', '$rootScope', '$timeout'];
	
	function SyntheseExpandedController(SyntheseService, PeriodService, PerimeterService, $state, $scope, $rootScope, $timeout) {
		var vm = this;
			
		// wait for the view to load.
		$scope.$on('$viewContentLoaded', onStateComplete);
		//listening for change in period
		$scope.$on('PERIOD_CHANGE', onPeriodChange);
		
		
		function onStateComplete(event) {
			var state = $state.current.name.split('.');
			vm.cardID = state[1];
			getData();
			$scope.$emit('SYNTHESE_CARD_CHANGE', vm.cardID);
		}
		
		function onPeriodChange() {
			getData();
		}
		
		function getData() {
			var params = {
				period: PeriodService.current,
				perimeter: PerimeterService.current,
				card: vm.cardID
			};
			SyntheseService.querySignatures(params)
				.then(function(response) {
					vm.data = response.data;
					vm.signatures = response.data[vm.cardID].kpis;
					vm.pageTitle = response.data[vm.cardID].title;
					$timeout(function() {
						$scope.$broadcast('SYNTHESE_DATA_SIGNATURES_READY');
					}, 80);
			});
		}
		
		
	}
}())