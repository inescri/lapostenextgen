(function() {
	angular
		.module('zoop.laposte')
		.controller('SyntheseController', SyntheseController);
	
	SyntheseController.$inject = ['SyntheseService', 'PeriodService', 'PerimeterService', '$state', '$rootScope', '$scope', '$timeout'];
	
	function SyntheseController(SyntheseService, PeriodService, PerimeterService, $state, $rootScope, $scope, $timeout) {
		var vm = this;
		vm.expandCard = expandCard;
		vm.data = null;

		// wait for the view to load.
		$scope.$on('$viewContentLoaded', onStateComplete);
		$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
			vm.isLoadingState = true;
		});
		$scope.$on('PERIOD_CHANGE', onPeriodChange);
		$scope.$on('PERIMETER_CHANGE', onPerimeterChange);

		function onStateComplete(event) {
			vm.activeCard = null;
			getData();
		}

		function onPeriodChange() {
			getData();
		}
		
		function onPerimeterChange() {
			getData();
		}

		function getData() {
			var params = {
				period: PeriodService.current,
				perimeter: PerimeterService.current
			};
			vm.isLoadingState = true;
			
			SyntheseService.query(params)
			.then(function(response) {
				vm.isLoadingState = false;
				vm.data = response.data;
				// this is to give a little bit time before sending signal to the graph directives
				$timeout(function() {
					// this is telling the graph directives to attempt to render only when there is data available.
					$scope.$broadcast('SYNTHESE_DATA_READY');
				}, 500);
			})
		}
		
		function expandCard(card) {
			vm.activeCard = card;
			if (card == 'synthese') {
				$state.go('synthese', {});
			}
			$state.go('synthese-expanded.' + card , {});
		}
	}
	
}())