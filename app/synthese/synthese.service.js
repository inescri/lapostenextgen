(function() {
	angular
		.module('zoop.laposte')
		.service('SyntheseService', SyntheseService);
	
	SyntheseService.$inject = ['$http', '$q'];

	function SyntheseService($http, $q) {
		var service = this;
		
		// sample data
		DATASET = {
			isc: {score: generateRandomScore(2), trend: {me: generateRandomTrend(4), company:   generateRandomTrend(5), lineOne: "#aaa", lineTwo: "#fff"}, trendLabels: ['nov-16', 'dec-16','jan-17', 'fev-17', 'mar-17']},
			pro: {score: generateRandomScore(2), trend:{me: generateRandomTrend(2), company:   generateRandomTrend(3), lineOne: "#aaa", lineTwo: "#fff"},  trendLabels: ['jan-17', 'fev-17', 'mar-17']},
			banque: {score: generateRandomScore(2), trend: {me: generateRandomTrend(2), company:   generateRandomTrend(3), lineOne: "#aaa", lineTwo: "#fff" }, trendLabels: ['jan-17', 'fev-17', 'mar-17'],
				expanded: {
					data:  [generateRandomScore(), generateRandomScore(), generateRandomScore()], colors:  ["#fecb00", "#333","#fff"], categories: ["Satisfait", "Non satisfait", "Netrual"]
				}
			},
			financier: {score: generateRandomScore(2), trend: {me: generateRandomTrend(2), company:   generateRandomTrend(3), lineOne: "#aaa", lineTwo: "#4b7598"}, trendLabels: ['jan-17', 'fev-17', 'mar-17'],
				expanded: {
					data:  [generateRandomScore(), generateRandomScore(), generateRandomScore()], colors:  ["#fecb00", "#333","#999"], categories: ["Promoters", "Passives", "Detractors"]
				}},
			lbpComments: [{
				client: 'Catherine DE MEDECI', date: '15/02/2017',
				comment: 'Lorem ipsum. Bayang magiliw perlas ng silanganan. Alab ng puso \'di ka pasisiil. Sa dagat at bundok sa simoy at sa lingit mong bughaw. Aming ligaya na \'pag may mang-aapi.'
			}],

			proComments: [{
				client: 'Marie DE GUISE', date: '16/02/2017',
				comment: 'Lorem ipsum. Dalaga binata may asawa byuda. Itim puti pula asul kayumangi dilaw. Lintik lang ang walang ganti. Pikachu use thunderbolt.'
			}],

			financierComments: [{
				client: 'Jean BAPTISTE', date: '16/02/2017',
				comment: 'Lorem ipsum. Sit dolor amet. Run rabbit, run. If you the think the work is done it\'s time to dig another one. Breath, breath into the air.'
			}],
			recontact: generateRandomScore(),
			
			"clients-professionnels": {
				title: "Client professionnels",
				kpis: [{
					signature: 'Personnaliser la relation avec les clients Pros', 
					iscfocus: 'iscfocus',
					questions: [{ code: 'q19', title: 'Recommandation'}, { code: 'q8', title: 'Satisfaction globale'}]
				}, {
					signature: '1. Les 30 premi\350res secondes',
					questions: [
						{ code: 'q10', title: 'Accueil' }
					]
				}, {
					signature: '2. Servir les clients dans les meilleurs d\351lai',
					questions: [
						{ code: 'q13', title: 'Attente '}
					]
				}, {
					signature: '3. Etre encore plus attentifs \340 nos clients bancaires',
					questions: [
						{ code: 'q6', title: 'Offres bancaires' }
					]
				}, {
					signature: '4. Personnaliser la relation avec les clients Pros',
					questions: [
						{ code: 'q4', title: 'Conseillers Pro' },
						{ code: 'q15', title: 'Guichet Pro' },
						{ code: 'q16', title: 'Carte Pro' },
						{ code: 'q17', title: 'Conseil' },
						{ code: 'q18', title: 'Suivi' },
						{ code: 'q7', title: 'Services' }
					]
				}, {
					signature: '5. Etre pr\351sents \340 chaque instant',
					questions: [
						{ code: 'q11', title: 'Ecoute'},
						{ code: 'q12', title: 'Sens du service'}
					]
				}, {
					signature: '6. Proches, accessibles et connect\351s',
					questions: [
						{ code: 'q14', title: 'Automates'},
						{ code: 'q5', title: 'Offres en ligne'}
					]
				}, {
					signature: '7. L\'avis des nos clients nous importe',
					questions: [
						{ code: 'q22', title: 'Recontact'},
						{ code: 'q23', title: 'Recontact'}
					]
				}]
			},
			"banque-postale": {
				title: "La Banque Postale",
				kpis: [{
					signature: 'Etre encore plus attentifs à nos clients bancaires',
					iscfocus: 'iscfocus',
					questions: [{ code: 'q20', title: 'Recommandation'}, { code: 'q6', title: 'Satisfaction globale'}]
				}, {
					signature: '1. Les 30 premières secondes',
					questions: [
						{ code: 'q11', title: 'Bureau' },
						{ code: 'q13', title: 'Accueil' },
						{ code: 'q2', title: 'Satisfaction bureau' }
					]
				}, {
					signature: '2. Servir les clients dans les meilleurs d\351lai',
					questions: [
						{ code: 'q12', title: 'Pontualit\351' },
						{ code: 'q8', title: 'Joignabilit\351' },
						{ code: 'q9', title: 'Disponibilit\351' }
					]
				}, {
					signature: '3. Etre encore plus attentifs \340 nos clients bancaires',
					questions: [
						{ code: 'q16', title: 'Aboutissement' },
						{ code: 'q20-1', title: 'Recommandation' },
						{ code: 'q22', title: 'Fid\351lisation' },
						{ code: 'q23', title: 'Intention client'}
					]
				}, {
					signature: '5. Etre pr\351sents \340 a chaque instant',
					questions: [
						{ code: 'q14', title: 'Ecoute' },
						{ code: 'q15', title: 'Conseil'},
						{ code: 'q17', title: 'Suivi'}
					]
				}, {
					signature: '6. Proches, accessibles connect\351s',
					questions: [
						{ code: 'q10', title: 'Gestion des absences'}
					]
				}, {
					signature: '7. L\'avis des nos client nous importe',
					questions: [
						{ code: 'q24', title: 'Recontact'},
						{ code: 'q25', title: 'Recontact'},
					]
				}]
			}
		};
		
		service.query = queryData;
		service.querySignatures = queryDataSignatures;
		
		// synthese data without signatures
		function queryData(params) {
			return $http({
				method: 'GET',
				url: 'app/mock-data/synthese-data.json',
				params: params
			})
		}
		
		// synthese data with signatures
		function queryDataSignatures(params) {
			return $http({
				method: 'GET',
				url: 'app/mock-data/synthese-signature-' +params.card+ '-data.json',
				params: params
			})
		}
		
		///// DEMO ONLY /////
		/** method just to generate random scores **/
		function generateRandomScore(decimalPlaces) {
			decimalPlaces = (!decimalPlaces)? 0 : decimalPlaces;
			return parseFloat(Math.random() * 100).toFixed(decimalPlaces);
		}

		function generateRandomTrend(len, decimalPlaces) {
			var trend = [];
			for (var i=0; i<len; i++) {
				trend.push(generateRandomScore(decimalPlaces));
			}
			return trend;
		}
		/** end method for random scores **/
		
		function getPercentage(datarange) {
			var total = 0, percent = [];

			for (var i=0; i<datarange.length; i++) {
				var total = total + parseFloat(datarange[i]);
			}

			for (var i=0; i<datarange.length; i++) {
				var val = (parseFloat(datarange[i])/total) * 100;
				percent.push(parseFloat(val).toFixed(1) + '%');
			}

			return percent;
		}

	}
	
}())