(function() {
	angular.module('zoop.laposte')
		.directive('cardId', syntheseCard);
	
	function syntheseCard() {
		var directive = {
			link: linkfn,
			controller: function($state) {
				this.expandCard = function (card) {
					if (card == 'synthese') {
						$state.go('synthese', {});
					}
					$state.go('synthese-expanded.' + card , {});
				}
			}
		};
		
		return directive;
		
		function linkfn(scope, elem, attr, ctrl) {

			elem.on('click', function() {
				ctrl.expandCard(attr.cardId);
				scope.$broadcast('SYNTHESE_CARD_CHANGE', attr.cardId);
			});
			
			// adding 'active' class to the active card tile
			scope.$on('SYNTHESE_CARD_CHANGE', function (evt, x) {
				$('.card-tile, .card-tile-micro').removeClass('active');
				$('[card-id="'+x+'"]').addClass('active');
			})
		}
		
	}
}())