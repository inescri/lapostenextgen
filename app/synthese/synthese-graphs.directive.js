(function() {
	angular
		.module('zoop.laposte')
		.directive('syntheseTrendLines', syntheseTrendLines)
		.directive('syntheseBanquePie', syntheseBanquePie)
		.directive('syntheseFinancierPie', syntheseFinancierPie)
		.directive('syntheseSignatureTrend', syntheseSignatureTrend);
	
	function syntheseTrendLines() {
		var directive = {
			link: linkfn,
			scope: {
				data: '=',
				labels: '='
			}
		};
		
		return directive;
		
		function linkfn(scope, element) {
			scope.$on('SYNTHESE_DATA_READY', function() {
				createTrends(element[0], scope.data, scope.labels);
			})
			
		}
			
		function createTrends(container, data, labels) {

			var datasets = []; 

			// adjust this part whenver nescesary to add new lines
			if (data.me) {
				datasets.push({
					label: 'Votre selection',
					lineTension: 0,
					backgroundColor: "rgba(255,255,255,0.0)",
					borderColor: "rgba(255,255,255,0.8)",
					fill: true,
					pointRadius: 4,
					pointBorderWidth: 2,
					pointBackgroundColor: '#4b7598',
					data: data.me
				});
			}

			if (data.company) {
				datasets.push({
					label: 'National',
					lineTension: 0,
					backgroundColor: "rgba(255,255,255,0.0)",
					borderColor: "rgba(170,170,170,1)",
					fill: true,
					pointRadius: 4,
					pointBorderWidth: 2,
					pointBackgroundColor: '#4b7598',
					data: data.company
				});
			}

			var target = container.getContext('2d');
			var myChart = new Chart(target, {
				type: 'line',
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							ticks: {
								display: false,
								beginAtZero: true,
							}
						}],
						xAxes: [{
							ticks: {
								fontColor: '#fff',
								display: false
							}
						}]
					}
				},
				data: {
					labels: labels,
					datasets: datasets
				}
			});
		}
	}
	
	function syntheseBanquePie() {
		var directive = {
			link: linkfn,
			scope: {
				data: "="
			}
		}
		
		return directive;
		
		function linkfn(scope, element) {
			scope.$on('SYNTHESE_DATA_READY', function() {
				createBanquePie(element[0], scope.data)
			})
			
		}
		
		function createBanquePie(container, dataset) {
			if (!dataset) {return false;}
			var target = container.getContext('2d');
			var myChart = new Chart(target, {
			  type: 'doughnut',
			  options: {
				legend: {display: false, position: 'bottom', labels: {fontColor:'#fff', fontSize: 11, boxWidth: 15}},
				cutoutPercentage: 75,
				responsive: false
			  },
			  data: {
				labels: dataset.categories,
				datasets: [{
					borderColor: 'rgba(0,0,0,0)',
					backgroundColor: dataset.colors,
					data: dataset.data
				}]
			  }
			});
		}
	}

	function syntheseFinancierPie() {
		var directive = {
			link: linkfn,
			scope: {
				data: "="
			}
		}
		
		return directive;
		
		function linkfn(scope, element) {
			scope.$on('SYNTHESE_DATA_READY', function() {
				createFinancierPie(element[0], scope.data)
			})
			
		}
		
		function createFinancierPie(container, dataset) {
			if (!dataset) {return false;}
			var target = container.getContext('2d');
			var myChart = new Chart(target, {
			  type: 'doughnut',
			  options: {
				legend: {display: false, position: 'bottom', labels: {fontColor:'#fff', fontSize: 11, boxWidth: 15}},
				cutoutPercentage: 75,
				responsive: false
			  },
			  data: {
				labels: dataset.categories,
				datasets: [{
					borderColor: 'rgba(0,0,0,0)',
					backgroundColor: dataset.colors,
					data: dataset.data
				}]
			  }
			});
		}
	}
	

	function syntheseSignatureTrend() {
		
		var directive = {	
			scope: { data: '='},
			link: linkfn
		};
		
		return directive;
		
		function linkfn(scope, element) {
			scope.$on('SYNTHESE_DATA_SIGNATURES_READY', function() {
				createSignatureTrends(element[0], scope.data);
			});
		}
		
		function createSignatureTrends(target, data) {
			
			var myChart = new Chart(target, {
				type: 'line',
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							ticks: {
								display: true,
								beginAtZero: false,
							}
						}]
					}
				},
				data: {
					labels: data.categories,
					datasets: [{
						label: 'National',
						lineTension: 0,
						backgroundColor: "rgba(76,142,178,0.2)",
						borderColor: "rgba(76,142,178,1)",
						fill: false,
						data: data.national,
						pointRadius: 4,
						pointBorderWidth: 2,
						pointBackgroundColor: '#ffffff',
					}, {
						label: 'Agence',
						lineTension: 0,
						backgroundColor: "rgba(247,190,32,0.2)",
						borderColor: "rgba(247,190,32,1)",
						fill: false,
						data: data.me,
						pointRadius: 4,
						pointBorderWidth: 2,
						pointBackgroundColor: '#ffffff',
					}]
				}
			});
		}
	}
	
}())