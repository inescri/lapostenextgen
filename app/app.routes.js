(function() {
	angular
		.module('zoop.laposte')
		.config(config);
	
	config.$inject = ['$stateProvider', '$urlRouterProvider'];
	
	function config($stateProvider, $urlRouterProvider) {
		
		$urlRouterProvider.otherwise('/synthese');
		
		$stateProvider
			.state('synthese', {
				url: '/synthese',
				templateUrl: 'app/synthese/synthese.html',
				controller: 'SyntheseController as vm',
				group: 'synthese'
			})
			.state('synthese-expanded', {
				url: '/synthese-expanded',
				templateUrl: 'app/synthese/synthese-expanded.html',
				controller: 'SyntheseController as vm',
				group: 'synthese'
			})
			.state('synthese-expanded.banque-postale', {
				url: '/banque-postale',
				templateUrl: 'app/synthese/cards/banque-postale.html',
				controller: 'SyntheseExpandedController as vm',
				group: 'synthese'
			})
			.state('synthese-expanded.clients-professionnels', {
				url: '/clients-professionnels',
				templateUrl: 'app/synthese/cards/clients-professionnels.html',
				controller: 'SyntheseExpandedController as vm',
				group: 'synthese'
			})
			.state('synthese-expanded.centre-financier', {
				url: '/centre-financier',
				templateUrl: 'app/synthese/cards/centre-financier.html',
				group: 'synthese'
			})
			.state('observer-scores', {
				url: '/observer-scores?tab',
				templateUrl: 'app/observer/observer-scores.html',
				controller: 'ObserverScoresController as vm',
				group: 'observer'
			})
	}
	
}())