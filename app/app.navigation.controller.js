(function() {
	angular
		.module('zoop.laposte')
		.controller('AppNavigationController', AppNavigationController)
	
	AppNavigationController.$inject = ['$scope', '$state'];
	
	function AppNavigationController($scope, $state) {
		var vm = this;
		vm.state = $state;
		vm.isActiveClass = isActiveClass;
		
		$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			vm.currentPage = vm.state.current.group;
		});
		
		function isActiveClass(name) {
			if (name == vm.currentPage) {
				return 'active'
			}
			return false;
		}
	}
	
}())