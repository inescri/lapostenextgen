(function() {
	angular
		.module('zoop.laposte')
		.service('ObserverScoresService', ObserverScoresService)
	
	ObserverScoresService.$inject = ['$http'];
	
	function ObserverScoresService($http) {
		var service = this;
		
		// sample only, this should be http calls
		service.DATASET_global = {
			grouping: 'bureau',
			data: [{
				indiceClient: 50.7,
				globalQualiteServie: 56.3,
				globalQualitePercue: 25.0,
				profil: null
			}]
		}
		
		service.queryData = queryData;
		
		function queryData(param) {
			return service['DATASET_'+ param];
		}
	}
	
}());