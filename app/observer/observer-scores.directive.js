(function() {
	angular
		.module('zoop.laposte')
		.directive('tileTab', tileTab);
	
	function tileTab() {
		var directive = {
			restrict: 'AE',
			link: linkfn
		};
		
		return directive;
		
		function linkfn(scope, elem, attr) {
			elem.on('click', function() {
				$('.tile-tabs>.tab').removeClass('active');
				elem.addClass('active');
			})
		}
	}
	
}())