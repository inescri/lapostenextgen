(function () {
	angular
		.module('zoop.laposte')
		.controller('ObserverScoresController', ObserverScoresController);
	
	ObserverScoresController.$inject = ['$scope', '$rootScope', '$stateParams', 'ObserverScoresService'];
	
	function ObserverScoresController($scope, $rootScope, $stateParams, ObserverScoresService) {
		$scope.$on('$viewContentLoaded', onStateComplete);
		var vm = this;
		
		vm.gridOptions = {
			columnDefs: [
				{ field: 'id', displayName: 'No Regate'},
				{ field: 'shop', displayName: 'Bureau'},
				{ field: 'indiceClient', displayName: 'Indice client'},
				{ field: 'globalQualiteServie', displayName: 'Global Qualit\351 servie'},
				{ field: 'globalQualitePercue', displayName: 'Global Qualit\351 percue'},
				{ field: 'profil', displayName: 'Profil'},
				{ field: 'rapport', displayName: 'Rapport'}
			]    
		  };
		
		function onStateComplete() {
			if (!$stateParams.tab) {
				vm.tab = 'global'
			} else {
				vm.tab = $stateParams.tab;
			}
			displayTable(vm.tab);
		}
		
		function displayTable(tab) {
			vm.data = ObserverScoresService.queryData(tab);
			console.log(vm.data);
		}
	}

}())