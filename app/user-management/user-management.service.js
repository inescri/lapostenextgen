(function() {
	angular
		.module('zoop.laposte')
		.service('UserManagementService', UserManagementService);
	
	function UserManagementService() {
		var service = this;
		
		var users = [{
			id: 1,
			name: 'Qualim\351trie',
			email: 'email@qualimetrie.net',
			username: 'qualimetrie',
			level: 'national',
			alerts: [{
				name: 'PRO - DR Alpes Maritimes',
				date_created: '1/21/2017',
				influx: 'PRO_DATA Base',
				level: 'dr',
				selections: ['Alpes Maritimes']
			},
			{
				name: 'LBP - DR Auvergne',
				date_created: '1/22/2017',
				influx: 'LPB_DATA Base',
				level: 'dr',
				selections: ['Auvergne']
			}]
		}, {
			id: 2,
			name: 'ZOOP',
			email: 'email@zoop.net',
			username: 'zoop',
			level: 'cf',
			alerts: []
		}];

		var influxes = [{
			code: 'PRO_DATA Base',
			name: 'Clients Professionels',
			levels: [
				{code: 'dr', name:'DR'}, 
				{code: 'dt', name: 'DT'},
				{code: 'secteur', name: 'SECTEUR'},
				{code: 'bureau', name: 'BUREAU'}]
			},
			{
			code: 'LPB_DATA Base', 
			name: 'Le Banque Postale',
			levels: [
				{code: 'dr', name:'DR'}, 
				{code: 'dt', name: 'DT'},
				{code: 'secteur', name: 'SECTEUR'},
				{code: 'bureau', name: 'BUREAU'}]
			},
			{
			code: 'cf',
			name: 'Centre Financier',
			levels: [{code: 'cf', name: 'Centre Financier'}]	
			},
			{
			code: 'syn',
			name: 'Synergie',
			levels: [{code: 'cf', name: 'Centre Financier'}]	
			}
		];

		var locations = [
			{code: 'alpes_maritimes', name: 'Alpes Maritimes'},
			{code: 'alsace', name: 'Alsace'},
			{code: 'aquitine', name: 'Aquitine Sud'},
			{code: 'auvergne', name: 'Auvergne'}
		];

		service.currentUser = users[0];
		
		service.listUsers = listUsers;
		service.getUser = getUser;
		service.getCurrentUserId = getCurrentUserId;
		service.listInfluxes = listInfluxes;
		service.listLocations = listLocations;
		
		function getUser(id) {
			for(var i = 0; i<users.length; i++) {
				if (users[i].id == id) {
					return users[i];
				}
			}
			return null;
		}
		
		function listUsers() {
			return users;
		}

		function listInfluxes() {
			return influxes;
		}

		function listLocations() {
			return locations;
		}
		
		function getCurrentUserId() {
			return service.currentUser.id;
		}
	}
	
}())