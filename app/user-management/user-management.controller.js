(function() {
	angular
		.module('zoop.laposte')
		.controller('UserManagementController', UserManagementController);
	
	UserManagementController.$inject = ['UserManagementService'];
	function UserManagementController(UserManagementService) {
		var vm = this;	
		
		vm.usrv = UserManagementService;
		vm.tabMode = 'default';
		vm.user = vm.usrv.getUser(vm.usrv.getCurrentUserId());
		vm.isTab = isTab;
		vm.setTab = setTab;
		vm.influxes = vm.usrv.listInfluxes();
		vm.locations = vm.usrv.listLocations();
		
		// init
		vm.current = { influx: {}, locations: {}};
		
		function isTab(tab) {
			return vm.tabMode == tab;
		}
		
		function setTab(tab) {
			vm.tabMode = tab;
		}
	}
	
}())