(function() {
 var app = angular.module('zoop.component');
  app.component('zperimeterLocation', {
      require: {
        'parent' : '^zperimeter'
      },
      restrict: 'E',
      transclude: true,
      templateUrl: 'app/component/location-picker/location-picker.html',
      controller: 'zperimeterLocationController',
      bindings: {
        current: '='    
      }
  });

  app.controller('zperimeterLocationController', function($scope, PerimeterService) {
    $scope.data = data;
    $scope.current = {
      region: 'National',
      locations: [],
      sector: ""
    };

    $scope.togglePicker = function() {
      $("#regionPicker").toggle("show");
    };

    $scope.pickRegion = function(region) {
      $scope.current.region = region.name;
      $scope.current.locations.length = 0;
      $scope.data.locations = region.locations;
      if (region.name == "National") {
        $scope.current.locations = region.locations;
        $scope.confirmPerimeter();
      };
    };

    $scope.pickLocation = function(location) {
      var index = $scope.current.locations.indexOf(location.name);
      if (index >= 0) {
        $scope.current.locations.splice( index, 1 );
      } else {
        $scope.current.locations.push(location.name);
      }
    };

    $scope.cancelSelection = function() {
      $scope.current = {
        region: 'National',
        locations: [{id: '0', name: 'National'}],
        sector: ""
      };
      $scope.data.locations = [];
      $scope.confirmPerimeter();
    };

    $scope.pickAllLocations = function() {
      $scope.current.locations = [];
      angular.forEach($scope.data.locations, function(location) {
        $scope.pickLocation(location);
        $scope.current.locations.push(location.name);
        location.selected = true;
      });
    };

    $scope.confirmPerimeter = function() {
      if ($scope.current.locations.length) {   
        $scope.togglePicker();
		PerimeterService.setPerimeter($scope.current);
      } else {
      }
    };
  });
})();

var data = {
  regions: [
    {id: '0', 
    name: 'National', 
    locations: [
    {id: '0', name: 'National', selected: true}
    ]
    },
    {id: '1', 
    name: 'DEX',
    locations: [
      {id: '0', name: 'CARROS', selected: false},
      {id: '1', name: 'Alpes Maritimes', selected: false}
      ]
    },
    {id: '2', name: 'CENTRE FINANCIER',
    locations: [
      { id: '0', name: 'Sud Est', selected: false}, 
      { id: '1', name: 'Marseille', selected: false}
      ]
    },
    {id: '3', name: 'DAST'},
    {id: '4', name: 'DR'},
    {id: '5', name: 'DT'},
    {id: '6', name: 'SECTEUR'},
    {id: '7', name: 'BUREAU'}
  ],
  locations: [],
  sectors: [
    { id: '0', name: 'DT'},
    { id: '1', name: 'Alpes'},
    { id: '2', name: 'Essonne'},
    { id: '3', name: 'Franche Comtre'}
  ]
};