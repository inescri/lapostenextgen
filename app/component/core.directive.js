(function() {
	angular
		.module('zoop.laposte')
		.directive('sliderTarget', sliderTarget)
		.directive('sliderUncollapsed', slideUncollapsed)
		.directive('stateChangeLoading', stateChangeLoading)
		.directive('ripple', ripple);
	
	function sliderTarget() {
		var directive = {
			restrict: 'A',
			link: linkfn,
			template: '<span class="slider-caret"><i class="zmdi-caret-down zmdi"></i></span>'
		};
		
		return directive;
		
		function linkfn(scope, element, attr) {
			console.log('slide target directive activated');
			element.on('click', function() {
				element.siblings(attr.sliderTarget).slideToggle();
			})
			
		}
	}
	
	function  slideUncollapsed() {
		var directive = {
			restrict: 'A',
			link: linkfn,
			transclude: true,
			template: '<span class="slider-caret"><i class="fa fa-plus"></i></span>'
		};
		return directive;
		
		function linkfn(scope, element, attr) {
			console.log('un-collapsed');
			element.siblings(attr.sliderTarget).hide();
		}
	}
	
	function stateChangeLoading() {
		var directive = {
			restrict: 'AE',
			template: '<div class="spinner"><i class="zmdi zmdi-hc-spin zmdi-hc-2x zmdi-spinner"></i></div>',
			link: linkfn
		};
		
		return directive;
		
		function linkfn(scope, element, attr) {
			
		}
	}

	function ripple() {
		var directive = {
			restrict: 'AE',
			link: linkfn
		};
		
		return directive;
		
		function linkfn(scope, elem, attr) {
			elem.on('click', function() {
				var $div = $('<div/>'),
                      btnOffset = $(this).offset(),
                        xPos = event.pageX - btnOffset.left,
                        yPos = event.pageY - btnOffset.top;
                
                  
                  $div.addClass('ripple-effect');
                  var $ripple = $(".ripple-effect");
                  
                  $ripple.css("height", $(this).height());
                  $ripple.css("width", $(this).height());
                  $div
                    .css({
                      top: yPos - ($ripple.height()/2),
                      left: xPos - ($ripple.width()/2),
                      background: $(this).data("ripple-color")
                    }) 
                    .appendTo($(this));

                  window.setTimeout(function(){
                    $div.remove();
                  }, 500);
			})
		}
	}
	
}())