(function() {
	angular
		.module('zoop.component')
		.service('PeriodService', PeriodService);

	function PeriodService($rootScope) {
		var service = this;

		service.current = {
			start: '201701',
			end: '201701'
		};

		service.setPeriod = setPeriod;
		service.fetchAvailablePeriods = fetchAvailablePeriods;

		function setPeriod(periods) {
			service.current = periods;
			$rootScope.$broadcast('PERIOD_CHANGE', periods);
		}

		// fetch all period available
		function fetchAvailablePeriods() {

		}
	}
}())