(function() {
	var app = angular.module('zoop.component');
	app.component('zperimeter', {
		template: '' +
				   '<zperimeter-location></zperimeter-location>'+
				   '<zperimeter-month ' +
				   		'label-format="zperim.date.labelFormat" input-format="zperim.date.inputFormat" ' + 
				   		'start="zperim.date.start" end="zperim.date.end" ' +
				   		'min="zperim.date.min" max="zperim.date.max" ' +
				   		'on-date-change="zperim.setDate(date)" ' +
				   	'></zperimeter-month>',
		transclude: true,
		controller:  zPerimeterController,
		controllerAs: 'zperim',
		bindings: {
			/*this.location = {type: null, code: null}; */
			location : '<',
			/*this.date = {start: null,	end: null, format: null, labelFormat: null, min: null, max: null}*/
			date     : '<'
		}

	});

	function zPerimeterController () {
		this.setDate = function (pDate) {
			this.date.start = pDate.start;
			this.date.end = pDate.end;
		};
	};
})();

