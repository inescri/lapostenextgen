(function() {
	angular
		.module('zoop.component')
		.service('PerimeterService', PerimeterService);

	function PerimeterService($http, $rootScope) {
		var service = this;

		service.current = {
			type: 'SHOP',
			code: [100, 120]
		};

		service.setPerimeter = setPerimeter;
		service.fetchPerimeters = fetchPerimeters;

		function setPerimeter(perimeter) {
			service.current = perimeter;
			console.log(perimeter);
			$rootScope.$broadcast('PERIMETER_CHANGE', service.current);

		}

		// fetch available locations for the users
		function fetchPerimeters() {
			//$http.get('api/getLocations');
		}
	}
}())