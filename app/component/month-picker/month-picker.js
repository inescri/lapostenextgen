(function() {
	 var app = angular.module('zoop.component');
	app.component('zperimeterMonth', {
		templateUrl: 'app/component/month-picker/month-picker.html',
		controller: zPerimeterMonthController,
		controllerAs: 'zperimMonth',
		bindings: {
			labelFormat: '<',
			inputFormat: '<',
			start : '<',
			end   : '<',
			min   : '<',
			max   : '<',
			onDateChange : '&'
		}
		
	});

	function zPerimeterMonthController (PeriodService) {				
		var vm = this;

		vm.pickerDisplayed = false;
		vm.label = null;
		
		vm.internalFormat = 'YYYYMM';

		vm.monthsLabel = moment.months();
		vm.months = [];
		vm.years = [];

		vm.year = moment().format('YYYY');
		vm.month = { start: moment().format(vm.internalFormat), end: moment().format(vm.internalFormat) };
		vm.range = { min: '200001', max: '999912' };

		vm.model = { year: vm.year, start: vm.month.start, end: vm.month.end };

		vm.$onInit = onInit;
		vm.initModel = initModel;
		vm.initYears = initYears;
		vm.initMonths = initMonths;

		vm.limitMonth = limitMonth;

		vm.togglePicker = togglePicker;
		vm.onYearChange = onYearChange;
		vm.onMonthStartChange = onMonthStartChange;
		vm.onMonthEndChange = onMonthEndChange;
		vm.onApply = onApply;
		vm.onCancel = onCancel;

		vm.setDate = setDate;
		vm.setLabel = setLabel;

		function onInit() 
		{
			vm.month.start = moment(vm.start, vm.inputFormat).format(vm.internalFormat);
			vm.month.end   = moment(vm.end, vm.inputFormat).format(vm.internalFormat);
			vm.year        = moment(vm.start, vm.inputFormat).format('YYYY');

			if (vm.min != null) {
				vm.range.min = moment(vm.min, vm.inputFormat).format(vm.internalFormat);
			}

			if (vm.max != null) {
				vm.range.max = moment(vm.max, vm.inputFormat).format(vm.internalFormat);
			}

			vm.initModel();
			vm.initYears();
			vm.initMonths();
			vm.setLabel();
		}

		function initModel() 
		{
			vm.model.year  = vm.year;
			vm.model.start = vm.month.start;
			vm.model.end   = vm.month.end;
		}

		function initYears() 
		{
			var minYear = Math.floor(vm.range.min / 100);
			var maxYear = Math.floor(vm.range.max / 100);

			vm.years = [];
			for (var i = minYear ; i <= maxYear ; i++) {
				vm.years.push(i + '');
			}
		}

		function initMonths()
		{
			vm.months = [];

			var iMonth = vm.model.year + '01';
			var jMonth;
			for (var i = 0 ; i < 12 ; i++) {
				jMonth = vm.limitMonth(iMonth);
				if (jMonth == iMonth) {
					vm.months.push({value: iMonth + '', label: vm.monthsLabel[i]});				
				}

				iMonth++;
			}
		}

		function limitMonth(month)
		{
			if (month > vm.range.max) {
				return vm.range.max;
			}
			else if (month < vm.range.min) {
				return vm.range.min;
			}
			else {
				return month;
			}
		}

		function togglePicker($event)
		{
			console.log($event);
			vm.pickerDisplayed = vm.pickerDisplayed ? false : true;
			console.log(vm.pickerDisplayed);
		}

		function onYearChange()
		{
			vm.initMonths();
			vm.model.start = vm.limitMonth(vm.model.year + '' + vm.model.start.substr(4, 2));
			vm.model.end   = vm.limitMonth(vm.model.year + '' + vm.model.end.substr(4, 2));
		}

		function onMonthStartChange()
		{
			if (vm.model.start > vm.model.end) {
				vm.model.end = vm.model.start;
			}
		}

		function onMonthEndChange()
		{
			if (vm.model.start > vm.model.end) {
				vm.model.start = vm.model.end;
			}
		}

		function onApply()
		{
			vm.setDate();
			vm.pickerDisplayed = false;
		}

		function onCancel()
		{
			vm.initModel();			
			vm.onYearChange();
			vm.pickerDisplayed = false;
		}

		function setDate() {
			vm.month.start = vm.model.start;
			vm.month.end   = vm.model.end;
			PeriodService.setPeriod(vm.month);
			vm.setLabel();
		}

		function setLabel() {
			vm.label = moment(vm.start, vm.inputFormat).format(vm.labelFormat) +
						 ' - ' + moment(vm.end, vm.inputFormat).format(vm.labelFormat);

			vm.onDateChange({date: {start: vm.month.start, end: vm.month.end}});
		}
	};

})();
